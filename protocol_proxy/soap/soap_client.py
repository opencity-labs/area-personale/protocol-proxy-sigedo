from __future__ import annotations
import base64
import re
import xmltodict
from datetime import datetime, timedelta
from requests import Session, post
from zeep import Client, Plugin, Transport
from zeep.loader import load_external
from models.logger import get_logger
from models.process import Inserimento, Login, Protocollo
from models.excepitions import (
    LoginException,
    MainDocumentException,
    ProtocolloException,
)
from models.types import CallSoapMethod, HeaderContentType
from settings import DEBUG, WSDL_XSD_DIR

log = get_logger()


class SoapPlugin(Plugin):
    """
    Plugin di Zeep per memorizzare la richiesta e la risposta SOAP su log
    """

    def ingress(self, envelope, http_headers, operation):
        return envelope, http_headers

    def egress(self, envelope, http_headers, operation, binding_options):
        return envelope, http_headers


class SoapClient(object):
    def __init__(self, configuration: dict):
        self.token_url = configuration["token_url"]
        self.token = None
        self.token_timeout = datetime.utcnow() - timedelta(seconds=1)
        self.token_credential = (
            configuration["consumer_key"],
            configuration["consumer_secret"],
        )
        self.encoded_credential = (
            "Basic "
            + base64.b64encode(
                f"{configuration['username']}:{configuration['password']}".encode()
            ).decode()
        )

        self.soap_url = configuration["ws_url"]
        self.transport = Transport(session=Session())

        self._client = Client(
            f"{WSDL_XSD_DIR}/DOCAREAProtoSoapMTOM.wsdl",
            plugins=[SoapPlugin()],
            transport=self.transport,
        )

        self._load_schema_xsd()
        self._service = self._client.create_service(
            "{http://tempuri.org/}ProtocolloDocAreaServiceMTOMSoapBinding",
            self.soap_url,
        )

    def _get_bearer_token(self) -> str:
        """Richiede il token WSO2."""
        try:
            if not self.token or self.token_timeout >= datetime.utcnow():
                response = post(
                    url=self.token_url,
                    data={"grant_type": "client_credentials"},
                    auth=self.token_credential,
                )
                log.debug(f"Risposta token WSO2: {response.json()}")
                response.raise_for_status()

                token = response.json().get("access_token")
                if not token:
                    raise ValueError("Token non presente nella risposta JSON")
                self.token_timeout = datetime.utcnow() + timedelta(
                    seconds=response.json().get("expires_in")
                )
                log.debug(
                    f"Token WSO2 ottenuto: {token} con scadenza {self.token_timeout}"
                )
                self.token = "Bearer " + token
            return self.token
        except Exception as e:
            log.error(f"Errore durante la richiesta token WSO2: {e}", exc_info=DEBUG)
            return None

    def _post(self, data: bytes, call: CallSoapMethod):
        """
        Invia una richiesta POST con allegato al server SOAP.
        """
        headers = {"Authorization": self._get_bearer_token(), "SOAPAction": ""}
        if call == CallSoapMethod.LOGIN:
            log.debug("Chiamata a LOGIN")
            headers["Content-Type"] = HeaderContentType.TEXT_XML.value
        else:
            headers["Content-Type"] = HeaderContentType.SOAP_XML.value

        response = post(
            url=self.soap_url,
            data=data,
            headers=headers,
        )
        log.debug(f"Risposta dal server SOAP: {response.content.decode('utf-8')}")
        response.raise_for_status()
        xml_content = re.search(
            r"<soap:Envelope.*</soap:Envelope>", response.content.decode(), re.DOTALL
        ).group(0)
        response = xmltodict.parse(xml_content)
        return response

    def _load_schema_xsd(self):
        XSD_SEGNATURA = f"{WSDL_XSD_DIR}/segnatura_agspr.xsd"
        log.debug(f"Loading schema xsd: {XSD_SEGNATURA}")
        schema_doc = load_external(open(XSD_SEGNATURA, "rb"), None)
        doc = self._client.wsdl.types.create_new_document(
            schema_doc, f"file://{XSD_SEGNATURA}"
        )
        doc.resolve()

    def login(self, cod_ente: str, user: str, password: str) -> dict:
        """Effettua il login per ottenere il token Sigedo."""
        try:
            login_data = Login(cod_ente, user, password)
            soap_envelope: bytes = login_data.to_soap_envelope()
            response = self._post(soap_envelope, CallSoapMethod.LOGIN)
            return response["soap:Envelope"]["soap:Body"]["ns1:loginResponse"][
                "return"
            ]["strDST"]
        except Exception as e:
            log.error(f"Errore login Sigedo: {e}", exc_info=DEBUG)
            raise LoginException(e) from e

    def inserimento(self, inserimento_data: Inserimento) -> str:
        """Esegue l'inserimento utilizzando il token Sigedo."""
        try:
            soap_envelope: bytes = inserimento_data.to_soap_envelope()
            response = self._post(soap_envelope, CallSoapMethod.INSERIMENTO)
            response_content = response["soap:Envelope"]["soap:Body"][
                "ns1:inserimentoResponse"
            ]["return"]
            log.debug(f"Risposta inserimento: {response_content}")
            if response_content["lngErrNumber"] != "0":
                log.error("Errore durante l'inserimento", exc_info=DEBUG)
                raise MainDocumentException(response_content["strErrString"])

            return response_content
        except Exception as e:
            log.error(f"Errore inserimento allegato: {e}", exc_info=DEBUG)
            raise MainDocumentException(e) from e

    def protocollazione(self, protocollo: Protocollo) -> str:
        """
        Metodo per la protocollazione.
        Argomenti:
            protocollo (Protocollo): oggetto di tipo Protocollo
        Output:
            response (str): "Success" se il protocollo è stato inserito correttamente; in caso contrario, restituisce un oggetto soap-fault.
        """
        try:
            # Prepara il SOAP envelope
            soap_envelope: bytes = protocollo.to_soap_envelope()

            # Invia la richiesta di protocollazione
            response = self._post(soap_envelope, CallSoapMethod.PROTOCOLLAZIONE)
            response_content = response["soap:Envelope"]["soap:Body"][
                "ns1:protocollazioneResponse"
            ]["return"]

            log.debug(f"Risposta protocollazione: {response_content}")

            # Verifica se ci sono errori nella risposta
            if response_content["lngErrNumber"] != "0":
                log.error("Errore durante la protocollazione", exc_info=DEBUG)
                raise ProtocolloException(response_content["strErrString"])

            return response_content
        except Exception as e:
            log.error(f"Errore durante la protocollazione: {e}", exc_info=DEBUG)
            raise ProtocolloException(e) from e
