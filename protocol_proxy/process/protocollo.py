from __future__ import annotations
import datetime
from typing import List
from soap.soap_client import SoapClient
from storage.storage_manager import StorageManager
from process.intestazione import get_intestazione
from process.attachments import get_descrizione
from models.signature_schema.applicativo_protocollo import (
    ApplicativoProtocollo,
    Parametro,
)
from models.excepitions import LoginException, ProtocolloException
from models.process import Protocollo
from models.signature_schema.segnatura import Segnatura
from models.signature_schema.descrizione import Descrizione
from models.types import FlowState, Parameter, TrasmissionType
from models.logger import get_logger
from settings import DEBUG

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()

storage_manager = StorageManager()


def get_token(
    configuration: dict,
    client: SoapClient,
):
    log.debug(f"Login sul proxy: {configuration}")
    # login sul proxy
    token = client.login(
        configuration["codente"],
        configuration["username"],
        configuration["password"],
    )
    if not token:
        log.error(f"Errore login: {token}", exc_info=DEBUG)
        raise LoginException()
    return token


def process_message_protocollo(
    message: dict,
    configuration: dict,
    client: SoapClient,
    flow_message: FlowState,
):
    try:
        # login sul proxy
        token = get_token(configuration, client)
        transmission_type = message["registration_data"]["transmission_type"]
        segnatura: Segnatura = create_segnatura(
            message, client, token, configuration, transmission_type, flow_message
        )
        if not isinstance(segnatura, Segnatura):
            raise ProtocolloException()
        response_segnatura = client.protocollazione(
            Protocollo(configuration["username"], token, segnatura)
        )
        log.debug(response_segnatura)

        response_segnatura["n_fascicolo"] = ""
        if configuration["folder_number"]:
            response_segnatura["n_fascicolo"] = configuration["folder_number"]

        return response_segnatura, True
    except LoginException as le:
        log.error(f"Errore login: {le}", exc_info=DEBUG)
    except ProtocolloException as pe:
        log.error(f"Errore protocollo: {pe}", exc_info=DEBUG)
        return segnatura, False
    except Exception as e:
        log.error(f"Errore generico: {e}", exc_info=DEBUG)

    return None, False


def create_segnatura(
    message: dict,
    client: SoapClient,
    token: str,
    configuration: dict,
    transmission_type: str,
    flow_message: FlowState,
):
    log.debug("Creazione segnatura")
    applicativo_protocollo = get_applicativo_protocollo(
        transmission_type, configuration
    )
    intestazione = get_intestazione(message, transmission_type, configuration)
    try:
        descrizione = get_descrizione(
            message, client, token, configuration, flow_message
        )

        if not isinstance(descrizione, Descrizione):
            raise ProtocolloException()
        # Caso in cui otteniamo correttamente l'oggetto Descrizione
        segnatura: Segnatura = Segnatura(
            intestazione, descrizione, applicativo_protocollo
        )
        log.debug(segnatura)
        return segnatura

    except ProtocolloException as pe:
        log.error(f"Error creating Segnatura: {pe}", exc_info=DEBUG)
        return descrizione
    except Exception as e:
        log.error(f"General error creating Segnatura: {e}", exc_info=DEBUG)

    return None  # Return None for any failure in creating segnatura


def get_applicativo_protocollo(
    transmission_type: str, configuration: dict
) -> ApplicativoProtocollo:
    if transmission_type == TrasmissionType.INBOUND.value:
        parameter_name = Parameter.DATA_ARRIVO.value
    else:
        parameter_name = Parameter.DATA_SPEDIZIONE.value
    data = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

    parametri: List[Parametro] = []
    parametri.append(Parametro(parameter_name, data))
    parametri.append(Parametro("uo", configuration["id_unit_org"]))
    # for dispatch in configuration["dispatch_list"]:
    #     parametri.append(
    #         Parametro(
    #             "smistamento", f"{dispatch['code_unit']}@@{dispatch['dispatch_type']}"
    #         )
    #     )
    parametri.append(Parametro("utente", configuration["username"]))
    if "dispatch_type" in configuration and configuration["dispatch_type"]:
        parametri.append(Parametro("tipoSmistamento", configuration["dispatch_type"]))
    applicativo = ApplicativoProtocollo("AGSPR", parametri)
    return applicativo
