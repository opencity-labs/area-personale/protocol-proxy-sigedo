from __future__ import annotations
import base64
from typing import List
from soap.soap_client import SoapClient
from models.auth_token import AuthClient
from models.signature_schema.descrizione import Descrizione, Documento
from models.process import Inserimento
from models.excepitions import (
    AttachmentsException,
    MainDocumentException,
)
from models.types import FlowState
from models.logger import get_logger
from settings import DEBUG

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


def process_attachment_list(
    message: dict,
    client: SoapClient,
    token: str,
    configuration: dict,
    processed_attachments: List[Documento],
) -> List[Documento]:
    """
    Processa la lista di allegati.
    """
    log.debug("Caricamento allegati")
    tenant_id = message["tenant_id"]
    for attachment in message["attachments"]:
        # Esegui il processamento dell'allegato
        if search_attchment(attachment, processed_attachments):
            continue
        log.debug(f"Caricamento allegato: {attachment}")
        documento: Documento = get_attachment(
            attachment, client, token, configuration, tenant_id
        )
        if not documento:
            continue
        processed_attachments.append(documento)
        log.debug(f"Allegato {documento.nome} caricato correttamente")
    # controlla che la lista di allegati sia vuota
    return processed_attachments


def search_attchment(
    attachment: dict,
    processed_attachments: List[Documento],
) -> bool:
    """
    Cerca un allegato nella lista dei processati.
    """
    for processed_attachment in processed_attachments:
        if (
            processed_attachment.nome == attachment["name"]
            and processed_attachment.DescrizioneDocumento == attachment["description"]
        ):
            return True
    return False


def get_attachment(
    document: dict,
    client: SoapClient,
    token: str,
    configuration: dict,
    tenant_id: str,
) -> Documento:
    try:
        file_base64 = download_and_encode_pdf(document["url"], tenant_id)
        if file_base64 is None:
            return None
        document_insert = client.inserimento(
            Inserimento(configuration["username"], token, file_base64)
        )
        if not document_insert:
            raise MainDocumentException()
        if not configuration["tipo_documento"]:
            attachment = Documento(
                str(document_insert["lngDocID"]),
                document["name"],
                document["description"],
            )
        else:
            attachment = Documento(
                str(document_insert["lngDocID"]),
                document["name"],
                document["description"],
                configuration["tipo_documento"],
            )
        log.debug(f"Creazione Allegato:{attachment}")
        return attachment
    except MainDocumentException as me:
        log.error(f"Errore nella creazione dell'allegato: {me}", exc_info=DEBUG)
        return None
    except Exception as e:
        log.error(f"Errore nella creazione dell'allegato: {e}", exc_info=DEBUG)
        return None


def download_and_encode_pdf(
    url: str,
    tenant_id: str,
) -> str:
    auth_client = AuthClient(tenant_id)
    # Effettua una richiesta GET per scaricare il contenuto del PDF dalla URL
    log.debug(f"Richiesta GET per scaricare il contenuto del PDF dalla URL: {url}")
    response = auth_client.download_file_with_auth(url)
    # Verifica se la richiesta è stata eseguita con successo (codice 200)
    if response.status_code == 200:
        # Ottieni il contenuto del PDF
        pdf_content = response.content
        # Converti il contenuto del PDF in base64
        pdf_base64 = base64.b64encode(pdf_content).decode("utf-8")
        return pdf_base64
    else:
        # Se la richiesta non è andata a buon fine, stampa un messaggio di errore
        log.error(
            f"Errore nella richiesta. Codice: {response.status_code}", exc_info=DEBUG
        )
        return None


def get_descrizione(
    message: dict,
    client: SoapClient,
    token: str,
    configuration: dict,
    flow_message: FlowState,
):
    try:
        process_attachment = []
        log.debug("Creazione Descrizione")
        if flow_message == FlowState.PARTIAL_REGISTRATION:
            main_document = Documento.from_dict(
                message["retry_meta"]["retry_attachments"]["main_document"]
            )
            for attachment in message["retry_meta"]["retry_attachments"]["attachments"]:
                process_attachment.append(Documento.from_dict(attachment))
        else:
            main_document: Documento = get_attachment(
                message["main_document"],
                client,
                token,
                configuration,
                message["tenant_id"],
            )
            if not main_document:
                raise MainDocumentException()

        process_attachment: List[Documento] = process_attachment_list(
            message, client, token, configuration, process_attachment
        )
        # Validate attachments processing
        if len(process_attachment) != len(message["attachments"]):
            raise AttachmentsException(
                "Number of processed attachments does not match."
            )

        return Descrizione(main_document, process_attachment)

    except MainDocumentException as mde:
        log.error(f"Error downloading main document: {mde}", exc_info=DEBUG)
    except AttachmentsException as ae:
        log.error(f"Error processing attachments: {ae}", exc_info=DEBUG)
    except Exception as e:
        log.error(f"General error creating Descrizione: {e}", exc_info=DEBUG)

    # Return details in case of partial failure
    return_dict = {
        "main_document": main_document.to_dict() if main_document else None,
        "attachments": (
            [attachment.to_dict() for attachment in process_attachment]
            if process_attachment
            else []
        ),
    }

    return return_dict  # Return either full or partial description data on failure
