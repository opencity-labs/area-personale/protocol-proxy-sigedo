import json
import pytest
from models.types import StorageType
from settings import STAGE
from storage.storage_manager import StorageManager

if STAGE == "local":
    HOST = "http://localhost:9000"
else:
    HOST = "http://minio:9000"

# Configurazione del Storage Manager
storage_manager = StorageManager(StorageType.S3.value, HOST)

# Carica la configurazione da un file
with open("tests/configuration.json", "r") as file:
    configuration = file.read()


def test_save_in_storage():
    assert storage_manager.save("test.json", configuration) is True


def test_read_in_storage():
    # Read the stored data from the storage manager
    stored_data = storage_manager.read("test.json")

    if stored_data is None:
        pytest.fail("Failed to read data: returned None.")

    try:
        stored_data = json.loads(
            json.loads(stored_data)
        )  # First remove string encoding, then parse to dictionary
    except json.JSONDecodeError as e:
        pytest.fail(f"Failed to decode stored data: {e}")

    try:
        configuration_dict = json.loads(configuration)
    except json.JSONDecodeError as e:
        pytest.fail(f"Failed to decode configuration: {e}")

    # Compare stored_data with the configuration
    assert stored_data == configuration_dict


def test_delete_in_storage():
    assert storage_manager.delete("test.json") is True
