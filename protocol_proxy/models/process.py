import base64
import xmltodict
from models.signature_schema.segnatura import Segnatura
from models.logger import get_logger
from xml.etree.ElementTree import Element, SubElement, tostring

log = get_logger()


class ProcessModel:
    def to_dict(self):
        # Costruisce un dizionario contenente le proprietà dell'oggetto
        return {key: value for key, value in self.__dict__.items()}


class Login(ProcessModel):
    def __init__(self, cod_ente: str, user: str, password: str):
        self.strCodEnte = cod_ente
        self.strUserName = user
        self.strPassword = password

    def to_soap_envelope(self) -> bytes:
        """
        Crea un envelope SOAP basato sui dati di login.
        Returns:
            bytes: L'envelope SOAP in formato bytes.
        """
        envelope = f"""
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
            <soapenv:Header/>
            <soapenv:Body>
                <tem:login xmlns:tem="http://tempuri.org/">
                    <strCodEnte>{self.strCodEnte}</strCodEnte>
                    <strUserName>{self.strUserName}</strUserName>
                    <strPassword>{self.strPassword}</strPassword>
                </tem:login>
            </soapenv:Body>
        </soapenv:Envelope>
        """
        log.debug(f"Richiesta XML: {envelope}")
        return envelope.encode("utf-8")  # Converti l'envelope in bytes


class Inserimento(ProcessModel):

    def __init__(self, user: str, token: str, attachment: str):
        self.strUserName = user
        self.strDST = token
        self.strAttachment = attachment

    def to_soap_envelope(self) -> bytes:
        # Crea l'elemento principale
        envelope = Element(
            "soapenv:Envelope",
            {
                "xmlns:soapenv": "http://schemas.xmlsoap.org/soap/envelope/",
                "xmlns:tem": "http://tempuri.org/",
            },
        )

        # Aggiungi il corpo e i dettagli dell'inserimento
        body = SubElement(envelope, "soapenv:Body")
        inserimento = SubElement(body, "tem:inserimento")

        SubElement(inserimento, "strUserName").text = self.strUserName
        SubElement(inserimento, "strDST").text = self.strDST
        SubElement(inserimento, "strAttachment").text = self.strAttachment

        # Converte in bytes
        log.debug(f"Richiesta XML: {envelope}")
        xml_bytes = tostring(envelope, encoding="utf-8")
        return xml_bytes


class Protocollo(ProcessModel):

    def __init__(self, user: str, token: str, protocollo: Segnatura):
        self.strUserName = user
        self.strDST = token
        log.debug(f"Protocollo: {protocollo.to_dict()}")
        xml = xmltodict.unparse(protocollo.to_dict()).encode("utf-8")
        self.strAttachment = base64.b64encode(xml).decode("utf-8")

    def to_soap_envelope(self) -> bytes:
        # Crea l'elemento principale
        envelope = Element(
            "soapenv:Envelope",
            {
                "xmlns:soapenv": "http://schemas.xmlsoap.org/soap/envelope/",
                "xmlns:tem": "http://tempuri.org/",
            },
        )

        # Aggiungi il corpo e i dettagli dell'inserimento
        body = SubElement(envelope, "soapenv:Body")
        protocollazione = SubElement(body, "tem:protocollazione")

        SubElement(protocollazione, "strUserName").text = self.strUserName
        SubElement(protocollazione, "strDST").text = self.strDST
        SubElement(protocollazione, "strAttachment").text = self.strAttachment

        # Converte in bytes
        log.debug(f"Richiesta XML: {envelope}")
        xml_bytes = tostring(envelope, encoding="utf-8")
        return xml_bytes
