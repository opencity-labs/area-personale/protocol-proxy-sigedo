from typing import List
from models.signature_schema.amministrazione import Amministrazione
from models.signature_schema.aoo import AOO
from models.signature_schema.indirizzo_telematico import IndirizzoTelematico
from models.signature_schema.persona import Persona


class Destinatario:
    def __init__(
        self,
        IndirizzoTelematico: IndirizzoTelematico | None = None,
        Telefono: List[str] | None = None,
        Fax: List[str] | None = None,
        IndirizzoPostale: str | None = None,
    ):
        self.IndirizzoTelematico = IndirizzoTelematico
        self.Telefono = Telefono if Telefono is not None else []
        self.Fax = Fax if Fax is not None else []
        self.IndirizzoPostale = IndirizzoPostale

    def to_dict(self) -> dict:
        dict = {}
        return dict


class DestinatarioAmministrazione(Destinatario):
    def __init__(
        self,
        Amministrazione: Amministrazione,
        AOO: AOO | None = None,
        IndirizzoTelematico: IndirizzoTelematico | None = None,
        Telefono: List[str] | None = None,
        Fax: List[str] | None = None,
        IndirizzoPostale: str | None = None,
    ):
        super().__init__(
            IndirizzoTelematico=IndirizzoTelematico,
            Telefono=Telefono,
            Fax=Fax,
            IndirizzoPostale=IndirizzoPostale,
        )
        self.Amministrazione = Amministrazione
        self.AOO = AOO

    def to_dict(self) -> dict:
        dict = super().to_dict()
        dict["Amministrazione"] = self.Amministrazione.to_dict()
        if self.AOO is not None:
            dict["AOO"] = self.AOO.to_dict()
        if self.IndirizzoTelematico is not None:
            dict["IndirizzoTelematico"] = self.IndirizzoTelematico.to_dict()
        if self.Telefono is not None:
            dict["Telefono"] = self.Telefono
        if self.Fax is not None:
            dict["Fax"] = self.Fax
        if self.IndirizzoPostale is not None:
            dict["IndirizzoPostale"] = self.IndirizzoPostale
        return dict


class DestinatarioPersona(Destinatario):
    def __init__(
        self,
        Persona: Persona,
        IndirizzoTelematico: IndirizzoTelematico | None = None,
    ):
        super().__init__(IndirizzoTelematico=IndirizzoTelematico)
        self.Persona = Persona

    def to_dict(self) -> dict:
        dict = super().to_dict()
        dict["Persona"] = self.Persona.to_dict()
        if self.IndirizzoTelematico is not None:
            dict["IndirizzoTelematico"] = self.IndirizzoTelematico.to_dict()
        if self.Telefono is not None:
            dict["Telefono"] = self.Telefono
        if self.Fax is not None:
            dict["Fax"] = self.Fax
        if self.IndirizzoPostale is not None:
            dict["IndirizzoPostale"] = self.IndirizzoPostale
        return dict
