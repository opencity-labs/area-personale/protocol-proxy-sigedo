from __future__ import annotations

from models.signature_schema.indirizzo_telematico import IndirizzoTelematico


class Persona:
    def __init__(
        self,
        Cognome: str,
        Nome: str,
        CodiceFiscale: str,
        Titolo: str | None = None,
        Identificativo: str | None = None,
        id_soggetto: str | None = None,
        id_recapito: str | None = None,
        id_contatto: str | None = None,
        IndirizzoTelematico: IndirizzoTelematico | None = None,
    ):
        self.Cognome = Cognome
        self.Nome = Nome
        self.CodiceFiscale = CodiceFiscale
        self.Denominazione = Nome + " " + Cognome
        self.Titolo = Titolo
        self.Identificativo = Identificativo
        self.IndirizzoTelematico = IndirizzoTelematico
        self.id_soggetto = id_soggetto
        self.id_recapito = id_recapito
        self.id_contatto = id_contatto

    def to_dict(self) -> dict:
        dict = {
            "@id": self.CodiceFiscale,
            "Nome": self.Nome,
            "Cognome": self.Cognome,
            "CodiceFiscale": self.CodiceFiscale,
            "Denominazione": self.Denominazione,
        }
        if self.Titolo is not None:
            dict["Titolo"] = self.Titolo
        if self.Identificativo is not None:
            dict["Identificativo"] = self.Identificativo
        if self.IndirizzoTelematico is not None:
            dict["IndirizzoTelematico"] = self.IndirizzoTelematico.to_dict()
        if self.id_soggetto is not None:
            dict["id_soggetto"] = self.id_soggetto
        if self.id_recapito is not None:
            dict["id_recapito"] = self.id_recapito
        if self.id_contatto is not None:
            dict["id_contatto"] = self.id_contatto
        return dict
