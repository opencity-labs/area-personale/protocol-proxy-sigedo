from __future__ import annotations

from models.signature_schema.descrizione import Descrizione
from models.signature_schema.applicativo_protocollo import ApplicativoProtocollo
from models.signature_schema.intestazione import Intestazione


class Segnatura:
    def __init__(
        self,
        Intestazione: Intestazione,
        Descrizione: Descrizione,
        ApplicativoProtocollo: ApplicativoProtocollo | None = None,
    ):
        self.Intestazione = Intestazione
        self.Descrizione = Descrizione
        self.ApplicativoProtocollo = ApplicativoProtocollo

    def to_dict(self) -> dict:
        dict = {
            "Segnatura": {
                "Intestazione": self.Intestazione.to_dict(),
                "Descrizione": self.Descrizione.to_dict(),
            }
        }
        if self.ApplicativoProtocollo is not None:
            dict["Segnatura"][
                "ApplicativoProtocollo"
            ] = self.ApplicativoProtocollo.to_dict()
        return dict
