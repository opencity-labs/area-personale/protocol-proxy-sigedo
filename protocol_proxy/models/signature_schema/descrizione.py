from __future__ import annotations
from typing import List


class Documento:
    def __init__(
        self,
        id: int,
        nome: str,
        DescrizioneDocumento: str | None = None,
        TipoDocumento: str | None = None,
    ):
        self.id = id
        self.nome = nome
        self.DescrizioneDocumento = DescrizioneDocumento
        self.TipoDocumento = TipoDocumento

    def to_dict(self) -> dict:
        dict = {"@id": self.id, "@nome": self.nome}
        if self.DescrizioneDocumento is not None:
            dict["DescrizioneDocumento"] = self.DescrizioneDocumento
        if self.TipoDocumento is not None:
            dict["TipoDocumento"] = self.TipoDocumento
        return dict

    @classmethod
    def from_dict(cls, dict_obj: dict):
        """
        Create a Documento object from a dictionary.

        :param dict_obj: Dictionary containing the Documento data
        :return: Documento instance
        """
        # Extract the mandatory fields
        id = dict_obj.get("@id")
        nome = dict_obj.get("@nome")

        # Extract optional fields with default value None if not present
        descrizione_documento = dict_obj.get("DescrizioneDocumento")
        tipo_documento = dict_obj.get("TipoDocumento")

        # Return a new Documento instance
        return cls(
            id=id,
            nome=nome,
            DescrizioneDocumento=descrizione_documento,
            TipoDocumento=tipo_documento,
        )


class Allegati:
    def __init__(self, documenti: List[Documento] = None):
        self.documenti = documenti or []

    def to_dict(self) -> dict:
        return {"Documento": [doc.to_dict() for doc in self.documenti]}


class Descrizione:
    def __init__(self, Documento: Documento, Allegati: List[Allegati] | None = None):
        self.Documento = Documento
        self.Allegati = Allegati

    def to_dict(self):
        dict = {
            "Documento": self.Documento.to_dict(),
        }
        if self.Allegati is not None:
            dict["Allegati"] = []
            for attachment in self.Allegati:
                documento = attachment.to_dict()
                dict["Allegati"].append({"Documento": documento})
        return dict
