from enum import Enum


class FlowState(Enum):
    DOCUMENT_CREATED = "DOCUMENT_CREATED"
    REGISTRATION_PENDING = "REGISTRATION_PENDING"
    REGISTRATION_COMPLETE = "REGISTRATION_COMPLETE"
    PARTIAL_REGISTRATION = "PARTIAL_REGISTRATION"
    REGISTRATION_FAILED = "REGISTRATION_FAILED"


class MethodApi(Enum):
    GET = "GET"
    POST = "POST"
    PUT = "PUT"
    DELETE = "DELETE"
    PATCH = "PATCH"
    TRACE = "TRACE"


class ContactType(Enum):
    SMTP = "smtp"
    URI = "uri"


class StorageType(Enum):
    AZURE = "azure"
    LOCAL = "local"
    S3 = "s3"


class TrasmissionType(Enum):
    INBOUND = "Inbound"
    OUTOUND = "Outbound"


class FluxType(Enum):
    ENTRATA = "E"
    USCITA = "U"


class Parameter(Enum):
    DATA_ARRIVO = "DATA_ARRIVO"
    DATA_SPEDIZIONE = "DATA_SPEDIZIONE"


class CallSoapMethod(Enum):
    LOGIN = "login"
    PROTOCOLLAZIONE = "protocollaZione"
    INSERIMENTO = "inserimento"


class HeaderContentType(Enum):
    TEXT_XML = "text/xml"
    SOAP_XML = "application/soap+xml"
    WWW_FORM_URLENCODED = "application/x-www-form-urlencoded"
